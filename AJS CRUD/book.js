/* ng-app - To define the application scope.
ng-controller - To define controller scope.
ng-model - To bind data with view and opposite.
$valid - To check the validation of the form.
ng-show - To display an entity only when a certain condition is satisfied.
ng-hide - To hide an entity only when a certain condition is satisfied.
ng-repeat - To repeat over the objects in array.
ng-click - To bind function with click event.
ng-submit - To bind function with form submit event.
We have used editBook() and deleteBook() function calls with the parameter $index which is actually the index of the elements inside the book array.
 */
(function(){
	//defining app name
	var app = angular.module('bookStoreApp',[]);

	//defining app controller
	app.controller('bookStoreController',['$scope',function($scope){
		$scope.books = [
			{title:"AngularJS",author:"Brad Green",cost:20},
			{title:"Head First EJB",author:"Kathy Sierra",cost:25},
			{title:"Professional Website Performance",author:"Peter Smith",cost:10}
		];


		$scope.book = {};

		//utility function definitions
		$scope.addBook = function(book){
			$scope.books.push(book);
			$scope.book = {};
		}

		$scope.editBook = function(index){
			$scope.editId = index;
			$scope.book = $scope.books[index];
		}

		$scope.updateBook = function(book){
			$scope.books[$scope.editId] = book;
			$scope.editId = undefined;
			$scope.book = {};
		}

		$scope.deleteBook = function(index){
			$scope.books.splice(index,1);
		}

	}]);
})();
