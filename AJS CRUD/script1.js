/* ng-app - To define the application scope.
ng-controller - To define controller scope.
ng-model - To bind data with view and opposite.
$valid - To check the validation of the form.
ng-show - To display an entity only when a certain condition is satisfied.
ng-hide - To hide an entity only when a certain condition is satisfied.
ng-repeat - To repeat over the objects in array.
ng-click - To bind function with click event.
ng-submit - To bind function with form submit event.
We have used editBook() and deleteBook() function calls with the parameter $index which is actually the index of the elements inside the book array.
 */
(function(){
	//defining app name
	app.controller('EmpController',['$scope',function($scope){
		$scope.Comps = [
			{cname:"OFS",cloc:"CHENNAI",ctype:"SERVICE"},
			{cname:"HONEYWELL",cloc:"MADURAI",ctype:"PRODUCT"},
			{cname:"TVS",cloc:"CHENNAI",ctype:"SERVICE"},
			{cname:"GOOGLE",cloc:"USA",ctype:"PRODUCT"},
			{cname:"IBM",cloc:"USA",ctype:"PRODUCT"}
		];


		$scope.Comp = {};

		//utility function definitions
		$scope.addComp = function(Comp){
			$scope.Comps.push(Comp);
			$scope.Comp = {};
			document.getElementById("Compform").style.display = "none";
		}

		$scope.editComp = function(index){
			$scope.editId = index;
			$scope.Comp = $scope.Comps[index];
						document.getElementById("Compform").style.display = "block";
		}

		$scope.updateComp = function(Comp){
			$scope.Comps[$scope.editId] = Comp;
			$scope.editId = undefined;
			$scope.Comp = {};
			document.getElementById("Compform").style.display = "none";
		}

		$scope.deleteComp = function(index){
			$scope.Comps.splice(index,1);
		}

})();


function Compformdis(){
	document.getElementById("Compform").style.display = "block";
}
