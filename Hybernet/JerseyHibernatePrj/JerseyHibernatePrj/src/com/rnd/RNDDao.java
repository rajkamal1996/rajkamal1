package com.rnd;

import java.util.List;

public class RNDDao {
	
	
	public List<RND> getRNDRecords()
	{
		HibernateUtil hibernateUtil = new HibernateUtil();
		hibernateUtil.openCurrentSession();
		List<RND> rnds = (List<RND>)hibernateUtil.getCurrentSession().createQuery("from RND").list();
		hibernateUtil.closeCurrentSession();
		
		return rnds;
	}
	
	public RND getRNDRecordByID(int id)
	{
		HibernateUtil hibernateUtil = new HibernateUtil();
		hibernateUtil.openCurrentSession();
		RND rnd = (RND)hibernateUtil.getCurrentSession().createQuery("from RND where id="+id).uniqueResult();
		hibernateUtil.closeCurrentSession();
		
		return rnd;
	}
	
	public void saveRND(RND r)
	{
		HibernateUtil hibernateUtil = new HibernateUtil();
		hibernateUtil.openCurrentSessionwithTransaction();
		hibernateUtil.getCurrentSession().save(r);
		hibernateUtil.closeCurrentSessionwithTransaction();
	}

	public static void main(String[] args) {
		RND r = new RND();
		r.setName("Kanishka");
		RNDDao rndDao = new RNDDao();
		rndDao.saveRND(r);
	}

}
