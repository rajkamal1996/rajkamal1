package com.rnd;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/rndService")
public class RNDController {
	
	@Path("/getRndRecords")
	@GET
	@Produces("application/json")
	public List<RND> getRNDRecord()
	{
		System.out.println("get RND Records called");
		RNDDao rndDao = new RNDDao();
		List<RND> records = rndDao.getRNDRecords();
		return records;
	}
	
	@Path("/saveRecord")
	@GET
	@Produces("text/html")
	@Consumes("text/html")
	public String  saveRNDRecord(
			@QueryParam("name") String myName
			)
	{
		RNDDao rndDao = new RNDDao();
		RND r = new RND();
		r.setName(myName);
		rndDao.saveRND(r);
		
		return "Record Inserted";
	}

}
