var employeeobjcts = [{
    "id": "1",
    "name": "Rajkamal",
    "type": "UI Developer"
  },
  {
    "id": "2",
    "name": "Vicky",
    "type": "Tester"
  },
  {
    "id": "3",
    "name": "Ramanathan",
    "type": "App Developer"
  },
  {
    "id": "4",
    "name": "SivaKaran",
    "type": "DB Admin"
  },
  {
    "id": "5",
    "name": "Aravind",
    "type": "IOS Developer"
  }
];

var companyobjects = [{
    "sno": "1",
    "cname": "Raju Software Technology",
    "loc": "Madurai"
  },
  {
    "sno": "2",
    "cname": "Google",
    "loc": "Banglore"
  },
  {
    "sno": "3",
    "cname": "OFS",
    "loc": "New-York"
  },
  {
    "sno": "4",
    "cname": "IBM",
    "loc": "Japan"
  },
  {
    "sno": "5",
    "cname": "Honeywell",
    "loc": "Banglore"
  },
  {
    "sno": "6",
    "cname": "TCS",
    "loc": "Delhi"
  }
];

var companyjsondata = [];
var employeejsondata = [];

//passing json obj comcomeditorcommandsn
localinitcompanydata(companyobjects);
localinitemployeedata(employeeobjcts);

//init local storage for company
function localinitcompanydata(comdata) {
  var comvalue = JSON.stringify(comdata);
  localStorage.setItem('companydatum', comvalue);
  feedcompanydata();
}
//load the company data from local storage
function feedcompanydata() {
  var companydatatemp = localStorage.getItem("companydatum");
  companyjsondata = JSON.parse(companydatatemp);
}

//init local storage for employee
function localinitemployeedata(empdata) {
  var empvalue = JSON.stringify(empdata);
  localStorage.setItem('employeedatum', empvalue);
  feedemployeedata();
}
//load the employee data from local storage
function feedemployeedata() {
  var employeedatatemp = localStorage.getItem("employeedatum");
  employeejsondata = JSON.parse(employeedatatemp);
}

function loadcompanytable() {
  document.getElementById("companyeditor").style.display = "none";
  document.getElementById("newcompanyeditor").style.display = "none";
  var companytable = '<tr></tr>';
  for (var i = 0; i < companyjsondata.length; i++) {
    companytable += "<tr><td width='4%'>" + companyjsondata[i].sno + "</td><td width='15%'>" + companyjsondata[i].cname + "</td><td width='15%'>" + companyjsondata[i].loc + "</td><td width='15%'><button type='button' class='btn btn-sm btn-success' onclick='editcompanyrecord(" + i + ")'><span class='glyphicon glyphicon-pencil'></span> </button> <button type='button'  class='btn btn-sm btn-danger' onclick='removecomany(" + i + ")'><span class='glyphicon glyphicon-trash'></span> </button></td>";
  }
  var newcomrecord = document.getElementById('addnewcomrecord');
  document.getElementById("companytable").innerHTML = companytable;
}




//LOAD EMPLOYEE

function loademployeetable() {
  document.getElementById("employeeeditor").style.display = "none";
  document.getElementById("newemployeeeditor").style.display = "none";
  var employeetable = '<tr></tr>';
  for (var i = 0; i < employeejsondata.length; i++) {
    employeetable += "<tr><td width='4%'>" + employeejsondata[i].id + "</td><td width='15%'>" + employeejsondata[i].name + "</td><td width='15%'>" + employeejsondata[i].type + "</td><td width='15%'><button type='button' class='btn btn-sm btn-success' onclick='editemployeerecord(" + i + ")'><span class='glyphicon glyphicon-pencil'></span></button> <button type='button'  class='btn btn-sm btn-danger' onclick='removeemployee(" + i + ")'><span class='glyphicon glyphicon-trash'></span></button></td>";
  }
  var newemprecord = document.getElementById('addnewemprecord');
  document.getElementById("employeetable").innerHTML = employeetable;
}








function editcompanyrecord(ce) {
  document.getElementById("companyeditor").style.display = "block";
  document.getElementById("newcompanyeditor").style.display = "none";
  document.getElementById('comidform').value = companyjsondata[ce].sno;
  document.getElementById('comnameform').value = companyjsondata[ce].cname;
  document.getElementById('composform').value = companyjsondata[ce].loc;
  var editclicks = '<center><button type="submit" class="btn btn-sm btn-success" id="comsubmitform" onclick="updatecom(' + ce + ')">update</button> <button class="btn btn-sm btn-danger" id="comeditcancel" onclick="cancelcomeditor()">Cancel</button></center>';
  document.getElementById("comeditorcommands").innerHTML = editclicks;
}



//EDIT EMPLOYEE

function editemployeerecord(ee) {
  document.getElementById("employeeeditor").style.display = "block";
  document.getElementById("newemployeeeditor").style.display = "none";
  document.getElementById('empidform').value = employeejsondata[ee].id;
  document.getElementById('empnameform').value = employeejsondata[ee].name;
  document.getElementById('empposform').value = employeejsondata[ee].type;
  var editclick = '<center><button type="submit" class="btn btn-sm btn-success" id="comsubmitform" onclick="updateemp(' + ee + ')">update</button> <button class="btn btn-sm btn-danger" id="comeditcancel" onclick="cancelempeditor()">Cancel</button></center>';
  document.getElementById("empeditorcommands1").innerHTML = editclick;
}




function updatecom(uc) {
  document.getElementById("companyeditor").style.display = "none";
  companyjsondata[uc].sno = document.getElementById("comidform").value;
  companyjsondata[uc].cname = document.getElementById("comnameform").value;
  companyjsondata[uc].loc = document.getElementById("composform").value;
  localinitcompanydata(companyjsondata);
  loadcompanytable();
}


//Update employee


function updateemp(ue) {
  document.getElementById("employeeeditor").style.display = "none";
  employeejsondata[ue].id = document.getElementById("empidform").value;
  employeejsondata[ue].name = document.getElementById("empnameform").value;
  employeejsondata[ue].type = document.getElementById("empposform").value;
  localinitemployeedata(employeejsondata);
  loademployeetable();
}




function cancelcomeditor() {
  document.getElementById("companyeditor").style.display = "none";
}


//cancel  employeeeditor


function cancelempeditor() {
  document.getElementById("employeeeditor").style.display = "none";
}

function removecomany(rc) {
  companyjsondata.splice(rc, 1);
  localinitcompanydata(companyjsondata);
  loadcompanytable();
}

//remove employee

function removeemployee(re) {
  employeejsondata.splice(re, 1);
  localinitemployeedata(employeejsondata);
  loademployeetable();
}

function addnewcom() {
  document.getElementById("newcompanyeditor").style.display = "block";
  document.getElementById("companyeditor").style.display = "none";
  var neweditclick = '<center><button class="btn btn-sm btn-success" id="comsubmitform" onclick="savenewcom()">Submit</button> <button class="btn btn-sm btn-danger" id="comeditcancel" onclick="cancelnewcomeditor()">Cancel</button></center>';
  document.getElementById("newcomeditorcommands").innerHTML = neweditclick;
}

//add new employee

function addnewemp() {
  document.getElementById("newemployeeeditor").style.display = "block";
  document.getElementById("employeeeditor").style.display = "none";
  var neweditclicks = '<center><button class="btn btn-sm btn-success" id="empsubmitform" onclick="savenewemp()">Submit</button> <button class="btn btn-sm btn-danger" id="empeditcancel" onclick="cancelnewempeditor()">Cancel</button> </center>';
  document.getElementById("newempeditorcommands1").innerHTML = neweditclicks;
}



function cancelnewcomeditor() {
  document.getElementById("newcompanyeditor").style.display = "none";
}

//Cancelnew company

function cancelnewempeditor() {
  document.getElementById("newemployeeeditor").style.display = "none";
}




function savenewcom() {
  var tempsno = document.getElementById("newcomidform").value;
  var tempname = document.getElementById("newcomnameform").value;
  var temploc = document.getElementById("newcomposform").value;
  var newcomdata = {
    "sno": tempsno,
    "cname": tempname,
    "loc": temploc
  };
  companyjsondata.push(newcomdata);
  localinitcompanydata(companyjsondata);
  loadcompanytable();
}



//save new employee

function savenewemp() {
  var tempid = document.getElementById("newempidform").value;
  var tempname = document.getElementById("newempnameform").value;
  var temptype = document.getElementById("newempposform").value;
  var newempdata = {
    "id": tempid,
    "name": tempname,
    "type": temptype
  };
  employeejsondata.push(newempdata);
  localinitemployeedata(employeejsondata);
  loademployeetable();
}




function employee(){
  document.getElementById("employeediv").style.display = "block";
    document.getElementById("companydiv").style.display = "none";
}

function company(){
  document.getElementById("companydiv").style.display = "block";
  document.getElementById("employeediv").style.display = "none";
}
