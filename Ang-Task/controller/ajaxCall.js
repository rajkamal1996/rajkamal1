$(document).ready(function() {
	$.ajax({
	type: "GET",
	url: "../data/datas.json",
	datatype: "JSON",
	contentType: "application/json; charset=utf-8",
		success : function(datareceive) {          
			var mytablejs=document.getElementById("mytable");
			var row,j;
			var n=Object.keys(datareceive).length;
			for(var i=0;i<n;i++){
				j=i+1;
				row = mytablejs.insertRow(j);
				var Id = row.insertCell();
				var firstName = row.insertCell();
				var lastName = row.insertCell();
				var actions = row.insertCell();
				Id.innerHTML=datareceive[i].id;
				firstName.innerHTML=datareceive[i].firstName;
				lastName.innerHTML=datareceive[i].lastName;
				actions.innerHTML="<button class=\"btn warning\" id=\"edit\">Edit</button>  <button class=\"btn danger\" id=\"delete\">Delete</button>";
				
			}
		},
		error: function(error) {
			alert("error");
		}
	});
});

$('#search').keydown(function(){
	$.getJSON("../data/datas.json",function(data){
		var search = $('#search').val();
		var regex = new RegExp(search, 'id');
		var output;
		$.each(data, function(key, val){
			if((val.id.search(regex) != -1) || (val.id.search(regex) != -1)){
				output += "<tr>";
				output += "<td id='"+key+"'>"+val.id+"</td>";
				output += "<td firstName='"+key+"'>"+val.firstName+"</td>";
				output += "<td lastName='"+key+"'>"+val.lastName+"</td>";
				output += "</tr>";
			}
		});
		$('tr').html(output);
	});
});

$(document).on('click','#delete',function(){
$.confirm({
    title: 'Confirm!',
    content: '1 row will be deleted!',
    buttons: {
        confirm: function () {
            $.alert('Deleted!');
			document.getElementById("mytable").deleteRow(1);
        },
        cancel: function () {
            $.alert('Canceled!');
        },
        somethingElse: {
            text: 'Something else',
            btnClass: 'btn-blue',
            keys: ['enter', 'shift'],
            action: function(){
                $.alert('Something else?');
            }
        }
    }
});
});

$(document).on('click','#edit',function(){
$.confirm({
    title: 'Prompt!',
    content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<label>ID </label>' +
    '<input type="number" placeholder="Id" class="Id" required />' +
	'<label>First Name </label>' +
    '<input type="text" placeholder="firstName" class="firstName form-control" required />' +
	'<label>Last Name </label>' +
    '<input type="text" placeholder="lastName" class="lastName form-control" required />' +
    '</div>' +
    '</form>',
    buttons: {
        formSubmit: {
            text: 'Submit',
            btnClass: 'btn-blue',
            action: function () {
                var Id = this.$content.find('.Id').val();
				var firstName = this.$content.find('.firstName').val();
				var lastName = this.$content.find('.lastName').val();
                if(!Id){
                    $.alert('provide a valid id');
                    return false;
                }
                $.alert('Your ID is ' + Id + '<br>Your First Name is ' + firstName + '<br>Your Last Name is ' + lastName);
            }
        },
        cancel: function () {
            //close
        },
    },
    onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
    }
});
});